//
//  Button.swift
//  M16NNdM17
//
//  Created by Abraam on 28.12.2021.
//
import UIKit
class Brutton:UIButton{
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    init(title:String) {
        super.init(frame: .zero)
        setTitle(title, for: .normal)
        setTitleColor(.white, for: .normal)
        backgroundColor = .darkGray
        layer.cornerRadius = 10
       
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
